from django.shortcuts import render,get_object_or_404
from .models import *
from django.http import HttpResponse
from django.template import loader
from django.http import Http404

def index(request):
    # return HttpResponse("Você é o pipoco !!!")


    # latest_question_list = Question.objects.order_by('-pub_date')[:5]
    # template = loader.get_template('polls/index.html')
    # context = {
    #     'latest_question_list': latest_question_list,
    # }
    # return HttpResponse(template.render(context, request))

    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)

# def detail(request, question_id):
#     return HttpResponse("You're looking at question %s." % question_id)

# def results(request, question_id):
#     response = "You're looking at the results of question %s."
#     return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)

# A função a baixo é pra teste de erro(personaliza erro de acordo com o que foi definido na função)
def detail(request, question_id):
    # forma 1
    # try:
    #     question = Question.objects.get(pk=question_id)
    # except Question.DoesNotExist:
    #     raise Http404("Question does not exist")
    # return render(request, 'polls/detail.html', {'question' : question})
    
    # A view levanta uma exceção Http404 se a enquete com ID requisitado não existir.

    # forma 2
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})

    # A função get_object_or_404() recebe um modelo do Django como primeiro argumento 
    # e um número arbitrário de argumentos chave, que ele passa para a função do módulo get().
    #  E levanta uma exceção Http404 se o objeto não existir.